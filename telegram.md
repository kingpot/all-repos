# Telegram-Gruppen
 
Eine Sammlung von diversen Telegram-Gruppen für Fragen, gemeinsame Abstimmung, Lernpartner-Suche, etc.

*Falls für eine LVA oder deine Studienrichtung noch keine Gruppe existiert, erstelle bitte eine und füge hier den Link hinzu. Falls du nicht weißt, wie das geht, melde dich in der OMN-Gruppe (siehe unten).*

### OMN-Gruppe

Für alle Dinge, die das OMN selbst betreffen.
https://t.me/joinchat/FGFedBNMoUkUMscg8a0fgQ

### Studienspezifische Gruppen
-----

### Embedded Systems

Allgemeine Gruppe für das Masterstudium Embedded Systems
https://t.me/joinchat/GBhA6A10jcCNxDJVaAdflA

### Automatisierung

Allgemeine Gruppe für das Masterstudium Automatisierung
https://t.me/joinchat/FGFedA9vF4CqLHcuyGPd0A

### LVA-spezifische Gruppen
-----

#### 389.165 Communication Networks 2

https://t.me/joinchat/FGFedBe5P-uG-fwb4dhlGg


#### 389.161 Communication Networks Seminar

https://t.me/joinchat/FGFedBZn8tiKNqv95FB-Rg


#### 191.109 Dependable Systems

https://t.me/joinchat/FGFedBZSbUhSLbjfy3Oiaw


#### 370.038 Labor Smart Grids

https://t.me/joinchat/FGFedBK_M-Ys6o-UxEaHvQ


#### 376.050 Mechatronische Systeme

https://t.me/joinchat/FGFedAs-PAt0j7dc_6Xrdw
